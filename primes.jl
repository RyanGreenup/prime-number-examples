function main(;verbose=true)
    N = 3*100*1000
    # Make sure to type as 64 bit int
    primes = Vector{Int}(undef, N)
    primes[1] = 2
    pc = 2
    count = 1
    while (count < N)
        pc += 1
        if is_prime(pc, primes, count)
            count += 1
            primes[count] = pc
        end
    end

    if verbose print(primes) end
end

function is_prime(pc, primes, count)
    for i in 1:count
        p = primes[i]
        if p^2 > pc
            return true
        elseif (pc % p) == 0
            return false
        end
    end
    return true
end

@time main(verbose=false)

