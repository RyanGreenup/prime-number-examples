import numpy as np
N = 3*100*1000

def main():
    primes = np.zeros(N)
    primes[0] = 2
    pc = 2
    count = 1
    while count  < N:
        pc+=1
        if is_prime(pc, primes, count):
            count += 1
            primes[count-1] = pc
    print(primes)


def is_prime(pc, primes, n):
    for i in range(n):
        p = primes[i]
        if p**2 > pc:
            return True
        else:
            if pc % p == 0:
                return False
    return True


main()
