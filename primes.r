N <- 3 * 1000 * 100

main <- function() {
  primes <- vector(mode = "numeric", length = N)
  primes[1] <- pc <- 2
  count <- 1
  while (count < N) {
    pc <- pc + 1
    if (is_prime(pc, primes, count)) {
      count <- count + 1
      primes[count] <- pc
    }
  }
  print(primes)
}


is_prime <- function(pc, primes, n) {
  for (i in seq_len(n)) {
    p <- primes[i]
    # if the squared value is greater, move on
    if (p^2 > pc) {
        return(TRUE)
    # If it's divisible, it's not prime
    } else if ((pc %% p) == 0) {
      return(FALSE)
    }
  }
  # Otherwise it's prime
  return(TRUE)
}

start <- Sys.time()
main()
end <- Sys.time()
print(paste("Time taken is:", round(end - start, 3)))
