#include <stdio.h>
#define N 300000

int is_prime(int pc, int* primes, int n) {

    for (int i = 0; i < n; ++i) {
        int p = primes[i];
        if (p*p > pc) {
            // if greater than square, must be prime
            return 1;
        }
        if ((pc % p) == 0) {
            return 0;
        }
    }
    return 1;
}

int main() {

    // Create primes Array on the stack
    int primes[N];

    // Need to fill with zeros on stack unlike Heap
    for (int i = 0; i < N/2; ++i) {
        primes[i] = 0;
    }

    // Set initial primes
    primes[0] = 2;
    primes[1] = 3;
    int n_primes = 2; // number of found primes
    int pc = 4;       // next value to test

    // Loop over prime candidates
    printf("2\n3\n");
    while (n_primes < N) {
        if (is_prime(pc, primes, n_primes)==1) {
            n_primes++;
            primes[n_primes - 1 ] = pc;
            printf("%i\n", pc);
        }
        pc++;
    }

}
