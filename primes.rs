fn main() {
    let mut count: u64 = 2;
    let mut primes: Vec<u64> = vec![2, 3];
    let mut prime_cand: u64 = 3;
    const N: u64 = 3*100*1000;


    while count < N {
        prime_cand += 1;

        if is_prime(prime_cand, &primes) {
            primes.append(&mut vec![prime_cand]);
            count+=1;
        }
    }

    fn is_prime(pc: u64, primes: &Vec<u64>) -> bool {
        for p in primes {
            if *p * *p > pc {
                return true;
            }
            if pc % p == 0 {
                return false;
            }
        }
        return true;
    }

    // Print them
    for i in primes {
        println!("{}",i)
    }

}


