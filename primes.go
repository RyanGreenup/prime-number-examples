package main

import (
	"fmt"
)

const N = 3*100*1000

func main() {
	primes := []int{2, 3}

	prime_candidate := 3
	for len(primes) < N {
		prime_candidate++

		if is_prime(prime_candidate, primes) {
			primes = append(primes, prime_candidate)
		}
	}

	fmt.Println(primes)
}

func is_prime(pc int, primes []int) bool {
	// Assume the value is prime

	// Try to prove it non-prime
	for _, p := range primes {

		if p*p > pc {
			// If sqrt can't be prime
            return true
		}

		if is_divisible(pc, p) {
			return false
		}
	}
	// If we couldn't find any divisors it must be prime
	return true

}

func is_divisible(pc int, p int) bool {
	if pc%p == 0 {
		return true
	} else {
		return false
	}
}
